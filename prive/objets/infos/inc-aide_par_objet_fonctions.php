<?php
/**
 * Fonctions du squelette associé
 *
 * @package Spip/inc-aide_par_objet/UI
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function apo_args(string $exec){
	$titre = _T("aide_par_objet:" . $exec . "_titre", array(), array('force' => ''));
	// une img class ou id sont demandés
	if (preg_match('/<img>(.*?)<\/img>/s', $titre, $match)){
		$img = $match[1];
		$titre = str_replace('<img>'.$match[1].'</img>', '', $titre);
	} else {
		$img = 'bulle-24.svg';
	}
	if (preg_match('/<class>(.*?)<\/class>/s', $titre, $match)){
		$class = $match[1];
		$titre = str_replace('<class>'.$match[1].'</class>', '', $titre);
	} else {
		$class = 'info';
	}
	if (preg_match('/<class_titre>(.*?)<\/class_titre>/s', $titre, $match)){
		$class_titre = $match[1];
		$titre = str_replace('<class_titre>'.$match[1].'</class_titre>', '', $titre);
	} else {
		$class_titre = '';
	}
	return [
		'img' => $img,
		'class' => $class,
		'class_titre' => $class_titre,
		'titre' => $titre
	];
}